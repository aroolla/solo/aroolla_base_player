# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

{
    'name': 'Aroolla Player',
    'version': '1.0',
    'category': 'Discuss',
    'summary': 'Player registration for Aroolla',
    'description': "",
    'website': 'https://odoosim.ch/cases/paper/admin/',
    'depends': ['website_event'],
    'installable': True,
    'application': False,
    'auto_install': True,
    'data': [
            'views/views.xml'
     ]
}
