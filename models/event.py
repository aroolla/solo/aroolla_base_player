# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

from odoo import models, api, fields

import logging

_logger = logging.getLogger(__name__)


class EventRegistration(models.Model):
    _inherit = 'event.registration'

    solo_username = fields.Char(string="Solo username", readonly=True)
    solo_password = fields.Char(string="Solo password", readonly=True)
    solo_group = fields.Char(string="Solo group")

    def create_properties(self, company):
        """


        """
        properties_vals = [
            {
                'category_id': self.env.ref('aroolla_game_roasteria_solo_data.product_category_beans_green').id,
                'valuation_account_code': '1210',
                'expense_account_code': '1210',
            },
            {
                'category_id': self.env.ref('aroolla_game_roasteria_solo_data.product_category_packaging').id,
                'valuation_account_code': '1220',
                'expense_account_code': '1220',
            },
            {
                'category_id': self.env.ref('aroolla_game_roasteria_solo_data.product_category_sales_pods').id,
                'valuation_account_code': '1260',
                'expense_account_code': '4000',
            },
        ]
        for category in properties_vals:
            vals = [{
                    'name': 'property_stock_valuation_account_id',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_stock_valuation_account_id'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': self.env['account.account'].search([
                        ('code', '=', category['valuation_account_code']), ('company_id', '=', company.id)]).id,
                    'type': 'many2one',
                    'company_id': company.id,
                },
                {
                    'name': 'property_account_expense_categ_id',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_account_expense_categ_id'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': self.env['account.account'].search([
                        ('code', '=', category['expense_account_code']), ('company_id', '=', company.id)]).id,
                    'type': 'many2one',
                    'company_id': company.id,
                },
                {
                    'name': 'property_stock_account_input_categ_id',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_stock_account_input_categ_id'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': self.env['account.account'].search([
                        ('code', '=', '4008'), ('company_id', '=', company.id)]).id,
                    'type': 'many2one',
                    'company_id': company.id,
                },
                {
                    'name': 'property_stock_account_output_categ_id',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_stock_account_output_categ_id'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': self.env['account.account'].search([
                        ('code', '=', '4008'), ('company_id', '=', company.id)]).id,
                    'type': 'many2one',
                    'company_id': company.id,
                },
                {
                    'name': 'property_account_income_categ_id',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_account_income_categ_id'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': self.env['account.account'].search([
                        ('code', '=', '3200'), ('company_id', '=', company.id)]).id,
                    'type': 'many2one',
                    'company_id': company.id,
                },
                {
                    'name': 'property_stock_journal',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_stock_journal'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': self.env['account.journal'].search([
                        ('code', '=', 'STJ'), ('company_id', '=', company.id)]).id,
                    'type': 'many2one',
                    'company_id': company.id,
                },
                {
                    'name': 'property_valuation',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_valuation'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': 'real_time',
                    'type': 'selection',
                    'company_id': company.id,
                },
                {
                    'name': 'property_cost_method',
                    'fields_id': self.env['ir.model.fields'].search([
                        ('name', '=', 'property_cost_method'), ('model', '=', 'product.category')]).id,
                    'res_id': 'product.category,%s' % category['category_id'],
                    'value': 'fifo',
                    'type': 'selection',
                    'company_id': company.id,
                },
            ]
            self.env['ir.property'].create(vals)
        _logger.info('Load COGS for current company {}'.format(company.name))


    @api.model
    def create(self, vals):
        event_registration = super(EventRegistration, self).create(vals)

        main_company = self.env.ref('base.main_company')

        # Create Player's company (and company partner)
        company_partner = self.env['res.partner'].create({'email': 'bot@aroolla.com',
                                                          'name': event_registration.name,
                                                          'is_player': False,
                                                          'is_company': True,
                                                          'parent_id': main_company.partner_id.id
                                                          })
        new_company_vals = dict(name='Aroolla {}'.format(company_partner.id),
                                partner_id=company_partner.id,
                                parent_id=main_company.id)
        company = self.env['res.company'].create(new_company_vals)

        company_partner.company_id = company.id

        # Create Player's Partner (and user through res_partner.create)
        new_partner_vals = {'email': event_registration.email,
                            'name': event_registration.name,
                            'is_player': True,
                            'is_company': False,
                            'parent_id': company_partner.id
                            }
        partner = self.env['res.partner'].create(new_partner_vals)

        # associate the newly created event_registration with new PLayer's partner
        event_registration.partner_id = partner.id
        event_registration.write({
            'solo_username': new_partner_vals['login'],
            'solo_password': new_partner_vals['password'],
        })

        chart_template_id = main_company.chart_template_id

        _logger.info('load CoA for current company {}'.format(company.name))
        #company_uid = self.get_company_uid(self.env, company.name)

        # CAUTION! The user is member of base.group_system
        user = self.env['res.users'].browse(new_partner_vals['user_id'])

        if not company.chart_template_id:
            chart_template_id.sudo(user).load_for_current_company(15.0, 15.0)

        # We create ir_property records to support COGS
        self.create_properties(company)


        # We consume default tours for the new user
        self.env['web_tour.tour'].sudo(user).create([
            {'name': 'sale_tour', 'user_id': user.id},
            {'name': 'account_tour', 'user_id': user.id},
            {'name': 'account_accountant_tour', 'user_id': user.id},
        ])

        # When CoA is installed, we need to remove the admin rights
        # so re-apply default_user groups
        user.groups_id = [(6, 0, self.env.ref('base.default_user').groups_id.ids)]

        # Remove the users from event (this is not optimal, but it does the job as users are magically added to the event group)
        user_admin = self.env.ref('base.user_admin').id
        group_event = self.env.ref('event.group_event_user')
        group_event.users = [(6, 0, [user_admin,])]
        # to create only the tour scenario
        states = self.env['aroolla_base_bot.usr_scn_state'].create_more_for_users(
            [user], 1, scenario_domain=[('type', '=', 'tour')])
        # to create immediately all 3 scenarios for testing
        # states = self.env['aroolla_base_bot.usr_scn_state'].create_more_for_users([user], 3)
        _logger.info(states)

        return event_registration
