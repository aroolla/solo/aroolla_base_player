# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json

from odoo import models
from odoo.http import request


class Http(models.AbstractModel):
    _inherit = 'ir.http'

    def session_info(self):
        info = super(Http, self).session_info()
        # Fake user admin status status for front end only
        # so that tour_service.js display tour tips for players
        if request.session.uid and request.env.user.partner_id.is_player:
            info['is_admin']=True
        return info
