# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

from . import res_partner
from . import event
from . import ir_http
