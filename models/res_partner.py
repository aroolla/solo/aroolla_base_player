# -*- coding: utf-8 -*-
# Part of Aroolla. See LICENSE file for full copyright and licensing details.

from odoo import models, api, fields

import logging

from odoo.tools import pycompat

_logger = logging.getLogger(__name__)


class Partner(models.Model):
    _inherit = 'res.partner'
    is_player = fields.Boolean(default=False)

    def gen_password(self):
        import random
        import string
        _PASSWORD_LEN = 12
        alphabet = string.ascii_letters + string.digits
        return ''.join(random.choice(alphabet) for i in range(_PASSWORD_LEN))

    @api.model_create_multi
    def create(self, vals_list):
        """
        Override Partner create to create automatically an user with auto generated password
        for players kind of partner (created on aroolla game event registration)

        :param vals_list:
        :return:
        """
        partners = super(Partner, self).create(vals_list)
        for partner, vals in pycompat.izip(partners, vals_list):
            if partner.is_player:
                password = self.gen_password()
                login = 'solo{}'.format(partner.id)
                _logger.debug(
                    'Creating user: {} | {} | {} | for {}:{}<{}>'.format(login, password, partner.email, partner,
                                                                         partner.name,
                                                                         partner.email))
                user = self.env['res.users'].sudo().create(
                    {'partner_id': partner.id,
                     'parent_id': partner.parent_id.id,
                     'company_id': partner.parent_id.company_id.id,
                     'company_ids': [(6, 0, [partner.parent_id.company_id.id])],
                     'login': login,
                     'password': password,
                     # default language is english
                     'lang': 'fr_CH'
                     }
                )
                # if groups_id is set in create it's overridden by default_user groups
                # so update groups_id after creation
                user.groups_id = [(4, self.env.ref('base.group_system').id)]
                # Pass back login details to caller (EventRegistration)
                vals['password'] = password
                vals['login'] = login
                vals['user_id'] = user.id
        return partners
